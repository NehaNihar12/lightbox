import { useEffect, useState } from "react";
import "./App.css";

import Popup from "./components/Popup";

function App() {
  const [popupButton, setPopupButton] = useState(false);
  const [focusedElementBeforePopup, setfocusedElementBeforePopup] =
    useState(null);

  return (
    <div className="App">
      <main>
        <h1>Click on the button to open a pop up</h1>
        <button>button2</button>
        <button
          id="popup-button"
          onClick={(e) => {
            setPopupButton(true);
            setfocusedElementBeforePopup(document.activeElement);
          }}
        >
          Open Popup
        </button>

        <Popup
          isOpen={popupButton}
          // callback
          onClose={(a, b) => {
            //alert("123");
            setPopupButton(false);
            focusedElementBeforePopup.focus();
            //console.log(focusedElementBeforePopup);
          }}
        >
          <h3>My Popup</h3>
          <p>You can add contents or elements inside</p>
          <a href="#">anchor1</a>
          <a href="#">anchor2</a>
          <a href="#">anchor3</a>
          <p style={{ width: "fitContent" }}>not focusable</p>
          <a href="#">anchor3</a>
        </Popup>
      </main>
    </div>
  );
}

export default App;
