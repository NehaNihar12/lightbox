import React, { useEffect, useRef } from "react";

import cross from "../images/cross.svg";
import "./popup.css";

const Popup = (props) => {
  useEffect(() => {
    const keyDownHandler = (event) => {
      if (event.key === "Escape") {
        //console.log(event.key);
        event.preventDefault();
        props.onClose();
      }
    };

    document.addEventListener("keydown", keyDownHandler);
    //cleanup event listener
    return () => {
      document.removeEventListener("keydown", keyDownHandler);
    };
  }, [props]);

  const popupRef = useRef(null);

  //listen and trap the keyboard for popup modal
  useEffect(() => {
    function openModal(e) {
      //all focusable element
      let focusableElString =
        "a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[tabindex='0'],[contenteditable]";
      let focusableElements =
        popupRef.current.querySelectorAll(focusableElString);

      //converting nodelist to array
      focusableElements = Array.prototype.slice.call(focusableElements);

      let firstTabStop = focusableElements[0];
      let lastTabStop = focusableElements[focusableElements.length - 1];
      //check for tab key press
      if (e.keyCode === 9) {
        //console.log(e.key);
        if (e.shiftKey) {
          if (document.activeElement === firstTabStop) {
            e.preventDefault();
            lastTabStop.focus();
          }
        } //TAB
        else {
          if (document.activeElement === lastTabStop) {
            e.preventDefault();
            firstTabStop.focus();
          }
        }
      }
    }
    document.addEventListener("keydown", openModal);
  }, []);

  //style on focus
  function styleOnFocus(e) {
    console.log(e.target);
    e.target.classList.add("focus");
  }

  return props.isOpen ? (
    <div id="popup-main">
      <div
        id="popup-inner"
        ref={popupRef}
        // onKeyDown={(e) => openModal(e)}
        onFocus={(e) => styleOnFocus(e)}
      >
        <header>
          <p>Header</p>
          <div>
            <img
              src={cross}
              alt="close-button"
              className="react-icon close-button"
              onClick={() => props.onClose(false)}
            ></img>
          </div>
        </header>
        {props.children}
        <footer></footer>
      </div>
    </div>
  ) : null;
};

export default Popup;
